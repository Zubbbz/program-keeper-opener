/*
	This program is intended to use a list of executables defined in an accompanying json file, and keep one instance of each open and attempt to restart any process that may die.

	Copyright (C) 2024 Nathan

	You can contact me by email at qjvzkv92@untrained4356.addy.io

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Portions of this software are based on or inspired by software originally
	licensed under the MIT License. The text of the MIT License is included in
	the file "ORIGINAL_LICENSE"
*/

use serde::{Deserialize, Serialize};
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::process::Command;
use std::thread;
use std::time::Duration;

#[derive(Serialize, Deserialize)]
struct ExecutableConfig {
	executables: Vec<String>,
}

fn read_executables_from_json(json_path: &str) -> Vec<String> {
	let mut file = File::open(json_path);

	let content = match &mut file {
		| Ok(f) => {
			let mut content = String::new();
			f.read_to_string(&mut content)
				.expect("Failed to read JSON file");
			Some(content)
		},
		| Err(_) => None,
	};

	match content {
		| Some(c) => {
			let config: ExecutableConfig =
				serde_json::from_str(&c).expect("Failed to parse JSON");
			config.executables
		},
		| None => {
			println!("Creating a dummy JSON file...");
			initialize_dummy_json(json_path);
			vec![] // Return an empty vector if the file is not found or can't be read
		},
	}
}

fn initialize_dummy_json(file_path: &str) {
	let dummy_config = ExecutableConfig {
		executables: vec!["path\\to\\dummy.exe".to_string()],
	};

	let mut file = OpenOptions::new()
		.create(true)
		.write(true)
		.truncate(true)
		.open(file_path)
		.expect("Failed to create JSON file");

	let dummy_config_json = serde_json::to_string_pretty(&dummy_config)
		.expect("Failed to serialize dummy JSON");
	file.write_all(dummy_config_json.as_bytes())
		.expect("Failed to write to JSON file");
}

// ...

fn main() {
	let json_file_path = "executables.json";

	// Read executables from JSON file or create a dummy if it doesn't exist
	let executables = read_executables_from_json(json_file_path);

	loop {
		// Run each executable in parallel
		for exe in &executables {
			match Command::new(exe).spawn() {
				| Ok(_) => {
					println!("Started process: {}", exe);
				},
				| Err(err) => {
					eprintln!("Failed to start {}: {}", exe, err);
				},
			}
		}

		// Optional: Add a delay
		thread::sleep(Duration::from_secs(5));
	}
}

// ...
